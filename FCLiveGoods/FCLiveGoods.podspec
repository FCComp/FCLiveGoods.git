Pod::Spec.new do |s|


s.name         = "FCLiveGoods"
s.version      = "1.1.5-fc"
s.summary      = "FCLiveGoods"
s.description  = <<-DESC
FCLiveGoods 直播带货
DESC

s.homepage     = "https://gitee.com/FCComp/FCLiveGoods"
# s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"

# s.license      = "MIT (example)"
s.license      = { :type => "MIT", :file => "LICENSE" }

s.author             = { "fczhouyou" => "zhouyou@sobey.com" }
# Or just: s.author    = "ZhouYou"
# s.authors            = { "ZhouYou" => "zhouyou@sobey.com" }
# s.social_media_url   = "http://twitter.com/ZhouYou"

# s.platform     = :ios
s.platform     = :ios, "9.0"

#  When using multiple platforms
# s.ios.deployment_target = "5.0"
# s.osx.deployment_target = "10.7"
# s.watchos.deployment_target = "2.0"
# s.tvos.deployment_target = "9.0"


s.source       = { :git => "https://gitee.com/FCComp/FCLiveGoods.git", :tag => "#{s.version}" }
s.source_files  = "FCLiveGoods/FCLiveGoods.framework/Headers/*.h"
# s.exclude_files = "Classes/Exclude"
# s.public_header_files = "Classes/**/*.h"

s.resource  = "FCLiveGoods/*.bundle"
# s.resources = "Resources/*.png"
# s.preserve_paths = "FilesToSave", "MoreFilesToSave"

s.vendored_frameworks = 'FCLiveGoods/*.framework'
# s.framework  = "SomeFramework"
# s.frameworks = "SomeFramework", "AnotherFramework"

# s.library   = "iconv"
# s.libraries = "iconv", "xml2"


# ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
#
#  If your library depends on compiler flags you can set them in the xcconfig hash
#  where they will only apply to your library. If you depend on other Podspecs
#  you can include multiple dependencies to ensure it works.

# s.requires_arc = true

# s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
#s.dependency "TMUserCenter"

s.dependency "FCBaseKit"
s.dependency "FCMobStat"
s.dependency "ReactiveObjC"
s.dependency "MJRefresh"
s.dependency "Masonry"
s.dependency "SVProgressHUD"
s.dependency "AFNetworking"
s.dependency "FMDB"
s.dependency "Toast"
s.dependency "MJExtension"
s.dependency "SDWebImage"
s.dependency "MBProgressHUD"
s.dependency "YYText"
s.dependency 'SocketRocket'#sokect
s.dependency 'YBImageBrowser/NOSD' #图片预览
s.dependency 'YBImageBrowser/VideoNOSD'  #视频预览
s.dependency 'FCPLPlayerKit'#七牛播放器
s.dependency 'JKCategories'
s.dependency "SAMKeychain"
s.dependency 'YYImage/WebP'
s.dependency 'FCEmotionKit'
s.dependency 'SVGAPlayer'
s.dependency 'IQKeyboardManager'
s.dependency 'PPNumberButton' #商品计数
s.dependency 'SKUDataFilter' #商品计数 

end
